package binar.academy.mymvvmpractice

import android.app.Application
import binar.academy.mymvvmpractice.repository.MainRepository
import binar.academy.mymvvmpractice.service.ApiClient
import binar.academy.mymvvmpractice.service.ApiHelper
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object MainModule {
    val Application.mainModule get() = module {
        viewModel { MainViewModel(get()) }
    }
}