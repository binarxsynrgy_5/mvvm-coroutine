package binar.academy.mymvvmpractice

import android.app.Application
import binar.academy.mymvvmpractice.repository.MainRepository
import binar.academy.mymvvmpractice.service.ApiClient
import binar.academy.mymvvmpractice.service.ApiHelper
import org.koin.dsl.module

object DataModule {
    val Application.dataModule get() = module {

        //DATABASE

        //API SERVICE
        single { ApiClient.instance }
        single { ApiHelper(get()) }

        //REPOSITORY
        factory { MainRepository(get()) }
    }
}